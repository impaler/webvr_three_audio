import * as THREE from 'three';
import { WEBVR } from './utils/WebVR';
import clickSound from '../assets/clickSound.wav';

let container;
let camera, scene, raycaster, renderer;
let room, crosshair, button, sound;

// hovering state
let wasHovering;

init();
animate();

function init() {
  container = document.createElement('div');
  document.body.appendChild(container);

  // scene
  scene = new THREE.Scene();
  scene.background = new THREE.Color(0x505050);

  // camera
  camera = new THREE.PerspectiveCamera(
    70,
    window.innerWidth / window.innerHeight,
    0.1,
    10,
  );
  scene.add(camera);

  // crosshair
  crosshair = new THREE.Mesh(
    new THREE.RingBufferGeometry(0.02, 0.04, 32),
    new THREE.MeshBasicMaterial({
      color: 'lime',
      opacity: 0.5,
      transparent: true,
    }),
  );
  crosshair.position.z = -2;
  camera.add(crosshair);

  // ambient light
  scene.add(new THREE.HemisphereLight(0x606060, 0x404040));

  // button
  button = new THREE.Mesh(
    new THREE.BoxBufferGeometry(0.45, 0.25, 0.1),
    new THREE.MeshLambertMaterial({ color: 'pink' }),
  );
  button.position.y = 1.8;
  button.position.z = -3;
  button.scale.x = 2;
  scene.add(button);

  // button hover sound
  const listener = new THREE.AudioListener();
  camera.add(listener);
  sound = new THREE.Audio(listener);
  const audioLoader = new THREE.AudioLoader();
  audioLoader.load(clickSound, function(buffer) {
    sound.setBuffer(buffer);
    sound.setVolume(1);
  });

  raycaster = new THREE.Raycaster();

  renderer = new THREE.WebGLRenderer({ antialias: true });
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.vr.enabled = true;
  container.appendChild(renderer.domElement);

  // EnterVR button
  document.body.appendChild(WEBVR.createButton(renderer));
}

function animate() {
  renderer.setAnimationLoop(render);
}

function render() {
  // find if we are hovering the button
  raycaster.setFromCamera({ x: 0, y: 0 }, camera);
  const intersects = raycaster.intersectObjects(scene.children);
  const isHovering = Boolean(intersects.find((i) => i.object === button));
  if (isHovering) {
    // set button colour to red
    button.material.color.set('red');

    // play the hover sound if we just starting hovering
    if (!wasHovering) {
      sound.play();
    }
  } else {
    // set button colour to white
    button.material.color.set('pink');
  }
  // update hovering state
  wasHovering = isHovering;

  renderer.render(scene, camera);
}
